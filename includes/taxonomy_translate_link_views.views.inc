<?php
/**
 * @file
 * Provide views handler for taxonomy_translate_link.module.
 */

/**
 * Implements hook_views_data().
 */
function taxonomy_translate_link_views_views_data() {
  $data = array();
  $data['taxonomy_term_data']['translate_term_link'] = array(
    'title' => t('Term translate link'),
    'help' => t('Provide a simple link to translate the term.'),
    'field' => array(
      'handler' => 'TaxonomyTranslateLinkViewsHandlerTranslateTermLink',
    ),
  );
  return $data;
}
